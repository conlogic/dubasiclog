..
 =============================================================================
 Title          : Utiltiies for basic logging

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-12-27

 Description    : Change log for package ``dubasiclog``.
 =============================================================================


=========
Changelog
=========


Version 1.3.2 (2021-12-27)
==========================

- Add support for Python 3.10.


Version 1.3.1 (2021-09-07)
==========================

- Make formatting of the documentation more consistent.


Version 1.3 (2021-09-07)
========================

- Add a :py:class:`Logger` method :py:meth:`debug_is_enabled` and
  corresponding module-global :py:func:`debug_is_enabled` to check whether
  debugging is enabled.


Version 1.2.1 (2021-08-17)
==========================

- Improve test coverage by using different non-container types within
  containers.

- Fix :py:func:`pretty_format` for mappings with string keys.


Version 1.2 (2021-08-17)
========================

- Rework tests to make them more Pythonic.

- Add a pretty format function :py:func:`pretty_format` to format values
  involving collections for pretty-printing.


Version 1.1 (2021-07-27)
========================

- New logic for debugging activation: whether debugging is enabled for a
  logger (via its :py:meth:`enable_debug`) is independent of the active
  maximal log level (set via :py:meth:`set_max_level`) for the logger.

- For backward compatibility debugging is also enabled for a logger by
  using ``None`` as argument for its :py:meth:`set_max_level` call.

- Add a method :py:meth:`disable_debug` to disable debugging messages.

- Redo all tests as simple proper unit tests.


Version 1.0.5 (2021-07-22)
==========================

- Let Git ignore generated package info.

- Fix info for last version in ``CHANGELOG``.


Version 1.0.4 (2021-07-04)
==========================

- Use more :py:mod:`pathlib` in the package tools.


Version 1.0.3 (2021-06-30)
==========================

- Fix development status (via ``classifiers`` for ``setup``).

- Drop support for Python 3.5, since ``f"..."``-strings are used (via
  ``classifiers`` for ``setup``).


Version 1.0.2 (2021-05-12)
==========================

- Migrate package tools to ``f"..."``-strings and :py:mod:`pathlib`.


Version 1.0.1 (2021-03-19)
==========================

- Customize Sphinx HTML theme.

- Proper Sphinx-compatible formatting of the ``CHANGELOG`` file.


Version 1.0 (2021-02-25)
========================

- Add unit tests for important :py:class:`Logger` methods.

- Add Sphinx-based documentation.


Version 0.2 (2021-01-13)
========================

- Add special facilities for debug logging to the basic logger, namely
  to enable debug logging and to emit specific debugging log messages.

- Keep the default maximal log level of basic loggers for the
  module-global one.


Version 0.1 (2021-01-08)
========================

- Initial version of the package: implement a basic logger to emit leveled
  messages to the console.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
