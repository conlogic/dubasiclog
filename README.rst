..
 =============================================================================
 Title          : Utiltiies for basic logging

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-07-27

 Description    : Read me for package ``dubasiclog``.
 =============================================================================


===========================================
``dubasiclog``: utilities for basic logging
===========================================

This package provides utilities for basic logging for the console.  Standard
log messages have a log level associated.  It can be controlled by a maximal log
level what log messages are printed - namely those whose log level is less or
equal to the maximal log level.

Beside this there are debugging log messages that printed by an extra method.
These messages are not affected by active maximal log level.  Instead of it,
printing of debugging messages is separately enabled or disabled.  On the
other hand, if printing of debugging log messages is enabled, all standard log
messages with any numerical log level are printed, too.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
