..
 =============================================================================
 Title          : Utiltiies for basic logging

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-02-13

 Description    : Change log for the package.
 =============================================================================


.. include:: ../CHANGELOG.rst


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
