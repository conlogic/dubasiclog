..
 =============================================================================
 Title          : Utiltiies for basic logging

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-02-13

 Description    : Master file for the documentation.
 =============================================================================


==============================================================================
Documentation for ``dubasiclog``
==============================================================================

.. toctree::
   :maxdepth: 2

   overview
   usage

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
