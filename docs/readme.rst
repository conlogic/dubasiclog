..
 =============================================================================
 Title          : Utiltiies for basic logging

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-02-13

 Description    : Read me file for the package.
 =============================================================================


.. include:: ../README.rst


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
