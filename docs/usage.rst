..
 =============================================================================
 Title          : Utiltiies for basic logging

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-09-12

 Description    : Description of the user interface.
 =============================================================================


===================
 The user interface
===================

Loading ``dubasiclog``
======================

This package ``dubasiclog`` is used by importing its single module,
:py:mod:`dubasiclog`.


The ``dubasiclog`` interface
============================

The :py:mod:`dubasiclog` module defines, first of all, a class
:py:class:`Logger` providing a simple logger that prints to STDOUT.

.. autoclass:: dubasiclog.Logger

This class has three methods to control what log messages are emitted:

.. automethod:: dubasiclog.Logger.set_max_level

.. automethod:: dubasiclog.Logger.enable_debug

.. automethod:: dubasiclog.Logger.disable_debug

The basic method of :py:class:`Logger` to emit a log message is

.. automethod:: dubasiclog.Logger.log

There is also a specialized one for emitting debugging log messages:

.. automethod:: dubasiclog.Logger.debug_log

There is another method to test whether for a given log level a log message
would be emitted:

.. automethod:: dubasiclog.Logger.is_active_level

Finally, there is a method to check whether debugging is enabled:

.. automethod:: dubasiclog.Logger.debug_is_enabled

The latter method is useful, for instance, to control the verbosity of
messages for programs called as subprocesses.

Beside this, a :py:mod:`dubasiclog`-global logger is provided, together with
its methods as :py:mod:`dubasiclog`-global functions:

.. py:data:: logger

   A module-global :py:class:`Logger` object.

.. py:function:: set_max_loglevel(max_level)

   Set the maximal log level of :py:data:`logger` to `max_level`.

.. py:function:: enable_debug_log()

   Enable debugging log messages for :py:data:`logger`.

.. py:function:: disable_debug_log()

   Disable debugging log messages for :py:data:`logger`.

.. py:function:: is_active_loglevel(level)

   :return: ``True`` iff `level` is an active log level for
            :py:data:`logger`.

.. py:function:: debug_is_enabled()

   :return: ``True`` iff debugging is enabled for :py:data:`logger`.

.. py:function:: log(level, **posargs, **kwargs)

   Emit a log message built from positional arguments in list `posargs` and
   keyword arguments in dictionary `kwargs` if `level` is active for
   :py:data:`logger`.

.. py:function:: debug_log(**posargs, **kwargs)

   Emit a debugging log message built from positional arguments in list
   `posargs` and keyword arguments in dictionary `kwargs` if debugging is
   active for :py:data:`logger`.

Finally, there is a function :py:func:`pretty_format` for formatting data for
pretty printing.

.. autofunction:: dubasiclog.pretty_format

It is especially useful to print data involving sequences (like lists or
tuples) or mappings (like dictionaries).


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
