# ============================================================================
# Title          : Utiltiies for basic logging
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-12-27
#
# Description    : See documentation string below.
#
# License        : LGPL3 <https://www.gnu.org/licenses/lgpl-3.0.en.html>.
#
# Copyright      : 2021 Dirk Ullrich <dirk.ullrich@posteo.de>
# ============================================================================

'''
Single module for package ``dubasiclog``.
'''


import collections.abc as colls_abc


# Parameters
# ==========

# Version of this package.
version = '1.3.2'


# Classes and functions
# =====================


class Logger(object):
    '''
    Logging facility: a logger to emit all log messages whose level is <=
    the value of :py:attr:`max_level`, or all message, if debugging is enabled
    via a ``True`` value for :py:attr:`debug_on`.
    '''
    def __init__(self, max_level=0, debug_on=False):
        '''
        Initialize a logger.  For a description see the documentation of
        :py:class:`Logger`.
        '''
        self.set_max_level(max_level)
        self.debug_on = debug_on


    def set_max_level(self, max_level):
        '''
        Set the maximal log level for this logger to `max_level`.
        '''
        # For backward compatibility a ``None`` value for `max_level` enables
        # debugging, too.
        if max_level is None:
            self.enable_debug()
        else:
            self.max_level = max_level


    def enable_debug(self):
        '''
        Enable debugging, i.e. emitting all log messages including the
        debugging ones.
        '''
        self.debug_on = True


    def disable_debug(self):
        '''
        Disable debugging, i.e. emitting debugging messages is suppressed.
        '''
        self.debug_on = False


    def is_active_level(self, level):
        '''
        Check whether log level `level` is active, i.e. whether a log message
        with level `level` would be emitted.

        :return: ``True`` if `level` is active.
        '''
        if self.debug_on:
            # Debugging is enabled -> `level` is, like all log levels,
            # active.
            is_act = True
        elif level is None:
            # Debugging is not enabled, but `level` is the debugging log
            # level -> 'level` is not active.
            is_act = False
        else:
            # Debugging is not enabled, and `level` is not the debugging log
            # level -> `level` is active if its value is <= the value of
            # :py:attr:`max_level`.
            is_act = level <= self.max_level

        return is_act


    def debug_is_enabled(self):
        '''
        :return: ``True`` iff debugging is enabled.
        '''

        return self.debug_on


    def log(self, level, *posargs, **kwargs):
        '''
        If log messages of level `level` are emitted, emit a log message built
        from positional arguments in list `posargs` and keyword arguments in
        dictionary `kwargs`.
        '''
        if self.is_active_level(level):
            print(*posargs, **kwargs)


    def debug_log(self, *posargs, **kwargs):
        '''
        If debug logging is enabled, emit a specific debug log message  built
        from positional arguments in list `posargs` and keyword arguments in
        dictionary `kwargs`.
        '''
        self.log(None, *posargs, **kwargs)


# For uniform logging both a logger and its methods that are essential to use
# that logger are provided globally.
logger = Logger()
set_max_loglevel = logger.set_max_level
enable_debug_log = logger.enable_debug
disable_debug_log = logger.disable_debug
is_active_loglevel = logger.is_active_level
debug_is_enabled = logger.debug_is_enabled
log = logger.log
debug_log = logger.debug_log


def pretty_format(data, indent=4, indent_level=0, prefix=None):
    '''
    Pretty-print data `data` involving collections using `indent`-many spaces
    for every indentation level.

    Number `indent_level` is level of indentation to start with.

    A not-``None`` prefix string `prefix` starts the pretty-print
    representation of `data` instead of a respective indentation.

    :return: the pretty-print representation of `data`.
    '''
    indent_prefix = ' ' * indent * indent_level
    # Initial indent.
    if prefix is not None:
        out_str = prefix
    else:
        out_str = indent_prefix
    if isinstance(data, str):
        # `data` is a string -> add its representation.
        out_str += repr(data)
    elif isinstance(data, colls_abc.Mapping):
        # `data` is a mapping.
        out_str = _pretty_format_mapping(
            data, '{', '}', indent, indent_level, prefix)
    elif isinstance(data, tuple):
        # `data` is a tuple.
        out_str = _pretty_format_sequence(
            data, '(', ')', indent, indent_level, prefix)
    elif isinstance(data, colls_abc.Sequence):
        # `data` is a list.
        out_str = _pretty_format_sequence(
            data, '[', ']', indent, indent_level, prefix)
    else:
        # `data` has another type -> add its string.
        out_str += str(data)

    return out_str


def _pretty_format_sequence(seq, beg, end, indent, indent_level, prefix):
    '''
    Helper function to pretty-print a sequence-like container `seq` using
    begin / end delimiters `beg` / `end`.

    Arguments `indent` `indent_level` and `prefix` have the same meaning like
    in :py:func:`pformat`.

    :return: the pretty-print representation of `seq`.
    '''
    indent_prefix = ' ' * indent * indent_level
    # Initial indent.
    if prefix is not None:
        out_str = prefix
    else:
        out_str = indent_prefix
    # - Opening delimiter.
    out_str += beg
    if len(seq) > 0:
        # - Adding the pretty-printed elements, if there ones.
        is_first = True
        for e in seq:
            if is_first:
                is_first = False
                out_str += '\n'
            else:
                out_str += ',\n'
            out_str += pretty_format(e, indent, indent_level + 1)
        out_str += f"\n{indent_prefix}"
    # - Closing delimiter.
    out_str += end

    return out_str


def _pretty_format_mapping(mapg, beg, end, indent, indent_level, prefix):
    '''
    Helper function to pretty-print a mapping `mapg` using begin / end
    delimiters `beg` / `end`.

    Arguments `indent` `indent_level` and `prefix` have the same meaning like
    in :py:func:`pformat`.

    :return: the pretty-print representation of `mapg`.
    '''
    indent_prefix = ' ' * indent * indent_level
    # Initial indent.
    if prefix is not None:
        out_str = prefix
    else:
        out_str = indent_prefix
    # - Opening delimiter.
    out_str += beg
    if len(mapg) > 0:
        # - Adding the pretty-printed items, if there ones.
        is_first = True
        for (k, v) in mapg.items():
            if is_first:
                is_first = False
                out_str += '\n'
            else:
                out_str += ',\n'
            out_str += indent_prefix
            k_str = repr(k) if isinstance(k, str) else k
            out_str += f"{' ' * indent}{k_str}"
            out_str += pretty_format(v, indent, indent_level + 1, ': ')
        out_str += f"\n{indent_prefix}"
    # - Closing delimiter.
    out_str += end

    return out_str


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
