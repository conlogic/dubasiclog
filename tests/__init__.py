# ============================================================================
# Title          : Utiltiies for basic logging
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-02-12
#
# Description    : See documentation string below.
#
# License        : LGPL3 <https://www.gnu.org/licenses/lgpl-3.0.en.html>.
#
# Copyright      : 2021 Dirk Ullrich <dirk.ullrich@posteo.de>
# ============================================================================

'''
Init module for the unit test package of package ``dubasiclog``.
'''


# This turns the ``tests`` directory into a Python package.  This eases the
# discovery of tests.


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
