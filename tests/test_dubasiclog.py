# ============================================================================
# Title          : Utiltiies for basic logging
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-09-07
#
# Description    : See documentation string below.
#
# License        : LGPL3 <https://www.gnu.org/licenses/lgpl-3.0.en.html>.
#
# Copyright      : 2021 Dirk Ullrich <dirk.ullrich@posteo.de>
# ============================================================================

'''
Unit tests for module :py:mod:`dubasiclog`.
'''


import io
import contextlib
import unittest

import dubasiclog


# Classes
# =======


class check_stdout(object):
    '''
    Context manager to catch STDOUT and to check it.
    '''
    def __init__(self, tc, exp_out):
        '''
        Initialize a context manager for test case `tc`, and expect `exp_out`
        at STDOUT.
        '''
        self.stdout_catcher = io.StringIO()
        self.cm = contextlib.redirect_stdout(self.stdout_catcher)
        self.tc = tc
        self.exp_out = exp_out


    def __enter__(self):
        '''
        Method to enter out context manager.
        '''
        self.cm.__enter__()


    def __exit__(self, ex_type, ex_obj, traceback):
        '''
        Method to exit our context manager.
        '''
        self.cm.__exit__(ex_type, ex_obj, traceback)
        res_out = self.stdout_catcher.getvalue()
        self.tc.assertSequenceEqual(res_out, self.exp_out)


class Logger_is_active_level_TC(unittest.TestCase):
    '''
    Tests for :py:meth:`is_active_level` for :py:class:`Logger`.
    '''


    def test_num_le_default(self):
        '''
        Test a numeric log level <= maximal log and logger defaults.
        '''
        logger = dubasiclog.Logger()
        is_act = logger.is_active_level(logger.max_level)
        self.assertTrue(is_act)


    def test_num_gt_default(self):
        '''
        Test a numeric log level > maximal log and logger defaults.
        '''
        logger = dubasiclog.Logger()
        is_act = logger.is_active_level(logger.max_level + 1)
        self.assertFalse(is_act)


    def test_None_default(self):
        '''
        Test a ``None`` log level and logger defaults.
        '''
        logger = dubasiclog.Logger()
        is_act = logger.is_active_level(None)
        self.assertFalse(is_act)


    def test_num_le_max(self):
        '''
        Test a numeric log level <= maximal log and a logger with maximal log
        level set.
        '''
        max_level = 13
        logger = dubasiclog.Logger(max_level)
        is_act = logger.is_active_level(max_level)
        self.assertTrue(is_act)


    def test_num_gt_max(self):
        '''
        Test a numeric log level > maximal log and a logger with maximal log
        level set.
        '''
        max_level = 13
        logger = dubasiclog.Logger(max_level)
        is_act = logger.is_active_level(max_level + 1)
        self.assertFalse(is_act)


    def test_None_max(self):
        '''
        Test a ``None`` log level and a logger with maximal log level set.
        '''
        logger = dubasiclog.Logger(max_level=13)
        is_act = logger.is_active_level(None)
        self.assertFalse(is_act)


    def test_num_le_debug_on(self):
        '''
        Test a numeric log level <= maximal log and a logger with debug on.
        '''
        logger = dubasiclog.Logger(debug_on=True)
        is_act = logger.is_active_level(logger.max_level)
        self.assertTrue(is_act)


    def test_num_gt_debug_on(self):
        '''
        Test a numeric log level > maximal log and a logger with debug on.
        '''
        logger = dubasiclog.Logger(debug_on=True)
        is_act = logger.is_active_level(logger.max_level + 1)
        self.assertTrue(is_act)


    def test_None_debug_on(self):
        '''
        Test a ``None`` log level and a logger with debug on.
        '''
        logger = dubasiclog.Logger(debug_on=True)
        is_act = logger.is_active_level(None)
        self.assertTrue(is_act)


    def test_num_le_max_debug_on(self):
        '''
        Test a numeric log level <= maximal log and a logger with maximal log
        level set and with debug on.
        '''
        max_level = 13
        logger = dubasiclog.Logger(max_level, True)
        is_act = logger.is_active_level(max_level)
        self.assertTrue(is_act)


    def test_num_gt_max_debug_on(self):
        '''
        Test a numeric log level > maximal log and a logger with maximal log
        level set and with debug on.
        '''
        max_level = 13
        logger = dubasiclog.Logger(max_level, True)
        is_act = logger.is_active_level(max_level + 1)
        self.assertTrue(is_act)


    def test_None_max_debug_on(self):
        '''
        Test a ``None`` log level and a logger with maximal log
        level set and with debug on.
        '''
        logger = dubasiclog.Logger(13, True)
        is_act = logger.is_active_level(None)
        self.assertTrue(is_act)


class Logger_enable_debug_TC(unittest.TestCase):
    '''
    Tests for :py:meth:`enable_debug` for :py:class:`Logger`.
    '''


    def test_default(self):
        '''
        Test with logger defaults.
        '''
        logger = dubasiclog.Logger()
        logger.enable_debug()
        self.assertTrue(logger.debug_on)


    def test_debug_on(self):
        '''
        Test with a logger with debug on.
        '''
        logger = dubasiclog.Logger(debug_on=True)
        logger.enable_debug()
        self.assertTrue(logger.debug_on)


    def test_debug_off(self):
        '''
        Test with a logger with debug off.
        '''
        logger = dubasiclog.Logger(debug_on=False)
        logger.enable_debug()
        self.assertTrue(logger.debug_on)


class Logger_disable_debug_TC(unittest.TestCase):
    '''
    Tests for :py:meth:`disable_debug` for :py:class:`Logger`.
    '''


    def test_default(self):
        '''
        Test with logger defaults.
        '''
        logger = dubasiclog.Logger()
        logger.disable_debug()
        self.assertFalse(logger.debug_on)


    def test_debug_on(self):
        '''
        Test with a logger with debug on.
        '''
        logger = dubasiclog.Logger(debug_on=True)
        logger.disable_debug()
        self.assertFalse(logger.debug_on)


    def test_debug_off(self):
        '''
        Test with a logger with debug off.
        '''
        logger = dubasiclog.Logger(debug_on=False)
        logger.disable_debug()
        self.assertFalse(logger.debug_on)


class Logger_debug_is_enabled(unittest.TestCase):
    '''
    Tests for :py:meth:`debug_is_enabled` for :py:class:`Logger`.
    '''


    def test_default(self):
        '''
        Test with logger defaults.
        '''
        logger = dubasiclog.Logger()
        debug_is_on = logger.debug_is_enabled()
        self.assertFalse(debug_is_on)


    def test_debug_on(self):
        '''
        Test with a logger with debug on.
        '''
        logger = dubasiclog.Logger(debug_on=True)
        debug_is_on = logger.debug_is_enabled()
        self.assertTrue(debug_is_on)


    def test_debug_off(self):
        '''
        Test with a logger with debug off.
        '''
        logger = dubasiclog.Logger(debug_on=False)
        debug_is_on = logger.debug_is_enabled()
        self.assertFalse(debug_is_on)


class base_Logger_stdout_TC(unittest.TestCase):
    '''
    Base class for testing :py_class:`Logger` methods that emit output to
    STDOUT.
    '''
    def assertStdoutEqual(self, exp_out):
        '''
        Context manager to assert that output to STDOUT is equal to
        `exp_out`.
        '''

        return check_stdout(self, exp_out)


class Logger_log_TC(base_Logger_stdout_TC):
    '''
    Tests for :py:meth:`log` from :py:class:`Logger`.
    '''


    def test_num_le_num_debug_off(self):
        '''
        Test a numeric log level <= maximal log and a logger with debug off.
        '''
        msg = 'Hello logging'
        logger = dubasiclog.Logger(debug_on=False)
        with self.assertStdoutEqual(msg + '\n'):
            logger.log(logger.max_level, msg)


    def test_num_gt_num_debug_off(self):
        '''
        Test a numeric log level > maximal log and a logger with debug off.
        '''
        msg = 'Hello logging'
        logger = dubasiclog.Logger(debug_on=False)
        with self.assertStdoutEqual(''):
            logger.log(logger.max_level + 1, msg)


    def test_None_num_debug_off(self):
        '''
        Test ``None`` log level and a logger with debug off.
        '''
        msg = 'Hello logging'
        logger = dubasiclog.Logger(debug_on=False)
        with self.assertStdoutEqual(''):
            logger.log(None, msg)


    def test_num_le_num_debug_on(self):
        '''
        Test a numeric log level <= maximal log and a logger with debug on.
        '''
        msg = 'Hello logging'
        logger = dubasiclog.Logger(debug_on=True)
        with self.assertStdoutEqual(msg + '\n'):
            logger.log(logger.max_level, msg)


    def test_num_gt_num_debug_on(self):
        '''
        Test a numeric log level > maximal log and a logger with debug on.
        '''
        msg = 'Hello logging'
        logger = dubasiclog.Logger(debug_on=True)
        with self.assertStdoutEqual(msg + '\n'):
            logger.log(logger.max_level + 1, msg)


    def test_None_num_debug_on(self):
        '''
        Test ``None`` log level and a logger with debug on.
        '''
        msg = 'Hello logging'
        logger = dubasiclog.Logger(debug_on=True)
        with self.assertStdoutEqual(msg + '\n'):
            logger.log(None, msg)


    def test_kwargs(self):
        '''
        Test for using a keyword argument.
        '''
        msg = 'Hello logging'
        end = '!'
        logger = dubasiclog.Logger()
        with self.assertStdoutEqual(msg + end):
            logger.log(logger.max_level, msg, end=end)


class Logger_debug_log_TC(base_Logger_stdout_TC):
    '''
    Tests for :py:meth:`debug_log` from :py:class:`Logger`.
    '''


    def test_debug_off(self):
        '''
        Test with a logger with debug off.
        '''
        msg = 'Hello logging'
        logger = dubasiclog.Logger(debug_on=False)
        with self.assertStdoutEqual(''):
            logger.debug_log(msg)


    def test_debug_on(self):
        '''
        Test with a logger with debug on.
        '''
        msg = 'Hello logging'
        logger = dubasiclog.Logger(debug_on=True)
        with self.assertStdoutEqual(msg + '\n'):
            logger.debug_log(msg)


    def test_kwargs(self):
        '''
        Test for using a keyword argument.
        '''
        msg = 'Hello logging'
        end = '!'
        logger = dubasiclog.Logger(debug_on=True)
        with self.assertStdoutEqual(msg + end):
            logger.debug_log(msg, end=end)


class pretty_format_TC(unittest.TestCase):
    '''
    Tests for function :py:func:`pretty_format`.
    '''
    def setUp(self):
        # Default indent value.
        self.indent = 4


    def test_str_defaults(self):
        '''
        Test a string using the defaults.
        '''
        s = 'foo'
        res_str = dubasiclog.pretty_format(s)
        exp_str = f"'{s}'"
        self.assertSequenceEqual(res_str, exp_str)


    def test_str_indent(self):
        '''
        Test a string using another indent level.
        '''
        s = 'foo'
        indent = 13
        res_str = dubasiclog.pretty_format(s, indent=indent)
        exp_str = f"'{s}'"
        self.assertSequenceEqual(res_str, exp_str)


    def test_str_indent_level(self):
        '''
        Test a string using another indent level.
        '''
        s = 'foo'
        indent_level = 2
        res_str = dubasiclog.pretty_format(s, indent_level=indent_level)
        exp_str = f"{' ' * self.indent * indent_level}'{s}'"
        self.assertSequenceEqual(res_str, exp_str)


    def test_str_prefix(self):
        '''
        Test a string using an alternative prefix.
        '''
        s = 'foo'
        prefix = '->'
        res_str = dubasiclog.pretty_format(s, prefix=prefix)
        exp_str = f"{prefix}'{s}'"
        self.assertSequenceEqual(res_str, exp_str)


    def test_str_indent_indent_level(self):
        '''
        Test a string using another indent and indent level.
        '''
        s = 'foo'
        indent = 3
        indent_level = 2
        res_str = dubasiclog.pretty_format(
            s, indent=indent, indent_level=indent_level)
        exp_str = f"{' ' * indent * indent_level}'{s}'"
        self.assertSequenceEqual(res_str, exp_str)


    def test_str_indent_level_prefix(self):
        '''
        Test a string using another indent level and prefix.
        '''
        s = 'foo'
        indent_level = 2
        prefix = '->'
        res_str = dubasiclog.pretty_format(
            s, indent_level=indent_level, prefix=prefix)
        exp_str = f"{prefix}'{s}'"
        self.assertSequenceEqual(res_str, exp_str)


    def test_str_indent_indent_level_prefix(self):
        '''
        Test a string using another indent, indent level and prefix.
        '''
        s = 'foo'
        indent = 3
        indent_level = 2
        prefix = '->'
        res_str = dubasiclog.pretty_format(
            s, indent=indent, indent_level=indent_level, prefix=prefix)
        exp_str = f"{prefix}'{s}'"
        self.assertSequenceEqual(res_str, exp_str)


    def test_empty_list_default(self):
        '''
        Test for an empty list using the defaults.
        '''
        res_str = dubasiclog.pretty_format([])
        exp_str = "[]"
        self.assertSequenceEqual(res_str, exp_str)


    def test_list_1str_default(self):
        '''
        Test for a list containing 1 string element and using the defaults.
        '''
        e = 'foo'
        res_str = dubasiclog.pretty_format([e])
        exp_str = f"[\n{' ' * self.indent}'{e}'\n]"
        self.assertSequenceEqual(res_str, exp_str)


    def test_list_1str_indent(self):
        '''
        Test for a list containing 1 string element and using another indent.
        '''
        e = 'foo'
        indent = 13
        res_str = dubasiclog.pretty_format([e], indent=indent)
        exp_str = f"[\n{' ' * indent}'{e}'\n]"
        self.assertSequenceEqual(res_str, exp_str)


    def test_list_1str_prefix(self):
        '''
        Test for a list containing 1 string element and using another prefix.
        '''
        e = 'foo'
        prefix = '->'
        res_str = dubasiclog.pretty_format([e], prefix=prefix)
        exp_str = f"{prefix}[\n{' ' * self.indent}'{e}'\n]"
        self.assertSequenceEqual(res_str, exp_str)


    def test_list_1str_1nr_default(self):
        '''
        Test for a list containing 1 string element and 1 number element and
        using the defaults.
        '''
        e1 = 'foo'
        e2 = 11
        res_str = dubasiclog.pretty_format([e1, e2])
        exp_str = f"[\n{' ' * self.indent}'{e1}',\n"
        exp_str += f"{' ' * self.indent}{e2}\n]"
        self.assertSequenceEqual(res_str, exp_str)


    def test_list_1str_1nr_indent(self):
        '''
        Test for a list containing 1 string element and 1 number element and
        using another indent.
        '''
        e1 = 'foo'
        e2 = 11
        indent = 13
        res_str = dubasiclog.pretty_format([e1, e2], indent=indent)
        exp_str = f"[\n{' ' * indent}'{e1}',\n"
        exp_str += f"{' ' * indent}{e2}\n]"
        self.assertSequenceEqual(res_str, exp_str)


    def test_empty_dict_default(self):
        '''
        Test for an empty dictionary using the defaults.
        '''
        res_str = dubasiclog.pretty_format({})
        exp_str = "{}"
        self.assertSequenceEqual(res_str, exp_str)


    def test_dict_1nr_str_default(self):
        '''
        Test for a dictionary containing 1 item with a number key and a string
        value and using the defaults.
        '''
        k = 1
        v = 'foo'
        res_str = dubasiclog.pretty_format({k: v})
        exp_str = f"{{\n{' ' * self.indent}{k}: '{v}'\n}}"
        self.assertSequenceEqual(res_str, exp_str)


    def test_dict_1nr_str_indent(self):
        '''
        Test for a dictionary containing 1 item with a number key and a string
        value and using another indent.
        '''
        k = 1
        v = 'foo'
        indent = 13
        res_str = dubasiclog.pretty_format({k: v}, indent=indent)
        exp_str = f"{{\n{' ' * indent}{k}: '{v}'\n}}"
        self.assertSequenceEqual(res_str, exp_str)


    def test_dict_1nr_str_prefix(self):
        '''
        Test for a dictionary containing 1 item with a number key and a string
        value and using another prefix.
        '''
        k = 1
        v = 'foo'
        prefix = '->'
        res_str = dubasiclog.pretty_format({k: v}, prefix=prefix)
        exp_str = f"{prefix}{{\n{' ' * self.indent}{k}: '{v}'\n}}"
        self.assertSequenceEqual(res_str, exp_str)


    def test_dict_1nr_str_1str_nr_default(self):
        '''
        Test for a dictionary containing 1 item with a number key and a string
        value and 1 item with a string key and a number value using the
        defaults.
        '''
        k1 = 1
        v1 = 'foo'
        k2 = 'bar'
        v2 = 23
        res_str = dubasiclog.pretty_format({k1: v1, k2: v2})
        exp_str = f"{{\n{' ' * self.indent}{k1}: '{v1}',\n"
        exp_str += f"{' ' * self.indent}'{k2}': {v2}\n}}"
        self.assertSequenceEqual(res_str, exp_str)


    def test_dict_1nr_str_1str_nr_indent(self):
        '''
        Test for a dictionary containing 1 item with a number key and a string
        value and 1 item with a string key and a number value using another
        indent.
        '''
        k1 = 1
        v1 = 'foo'
        k2 = 'bar'
        v2 = 23
        indent = 13
        res_str = dubasiclog.pretty_format({k1: v1, k2: v2}, indent=indent)
        exp_str = f"{{\n{' ' * indent}{k1}: '{v1}',\n"
        exp_str += f"{' ' * indent}'{k2}': {v2}\n}}"
        self.assertSequenceEqual(res_str, exp_str)


    def test_tuple_1nr_1str_default(self):
        '''
        Test for a tuple containing 1 number element or 1 string element and
        using the defaults.
        '''
        e1 = 43
        e2 = 'bar'
        res_str = dubasiclog.pretty_format((e1, e2))
        exp_str = f"(\n{' ' * self.indent}{e1},\n"
        exp_str += f"{' ' * self.indent}'{e2}'\n)"
        self.assertSequenceEqual(res_str, exp_str)


    def test_list_nested(self):
        '''
        Test for a list containing other containers.
        '''
        k11 = 'foo'
        v11 = 7
        e21 = 'bar'
        e22 = 23
        e3 = {}
        e4 = []
        res_str = dubasiclog.pretty_format([{k11: v11}, [e21, e22], e3, e4])
        exp_str = f"[\n{' ' * self.indent}{{\n"
        exp_str += f"{' ' * self.indent * 2}'{k11}': {v11}\n"
        exp_str += f"{' ' * self.indent}}},\n"
        exp_str += f"{' ' * self.indent}[\n"
        exp_str += f"{' ' * self.indent * 2}'{e21}',\n"
        exp_str += f"{' ' * self.indent * 2}{e22}\n"
        exp_str += f"{' ' * self.indent}],\n"
        exp_str += f"{' ' * self.indent}{{}},\n"
        exp_str += f"{' ' * self.indent}[]\n]"
        self.assertSequenceEqual(res_str, exp_str)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
