# ============================================================================
# Title          : Utiltiies for basic logging
#
# Classification : (>>>Classification<<<)
#
# Author         : (>>>USER_NAME<<<)
#
# Date           : (>>>ISO_DATE<<<)
#
# Description    : See documentation string below.
#
# License        : LGPL3 <https://www.gnu.org/licenses/lgpl-3.0.en.html>.
#
# Copyright      : 2021 Dirk Ullrich <dirk.ullrich@posteo.de>
# ============================================================================

(>>>POINT<<<)

# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
